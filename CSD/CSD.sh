#!/bin/bash

# Generate CSD FODs from diffusion MRI data

mrconvert -force -fslgrad bvecs bvals -stride 1,2,3,4  -export_grad_mrtrix dwi.b b4k_dwi.nii.gz dwi_dwi.mif
dwiextract -bzero -force  -grad dwi.b dwi_dwi.mif dwi_b0.mif

#create brain mask
mrconvert -force -stride 1,2,3,4  nodif_brain_mask.nii.gz - | mrtransform -force -template dwi_b0.mif - - | mrthreshold -force -abs 0.5 - dwi_brainmask.mif #function
maskfilter -force dwi_brainmask.mif dilate - | mrthreshold -force -abs 0.5 - dwi_brainmask_dilated.mif
maskfilter -force dwi_brainmask.mif -npass 2 erode - | mrthreshold -force -abs 0.5 - dwi_brainmask_eroded.mif

#DTI
dwi2tensor -force dwi_dwi.mif dwi_dt.mif

tensor2metric -force -fa - dwi_dt.mif | mrcalc - dwi_brainmask_eroded.mif -mult dwi_fa.mif
tensor2metric dwi_dt.mif -vector - | mrcalc - dwi_fa.mif -mult dwi_ev.mif

#Calculate wm mask
mrthreshold  -abs 0.2 dwi_fa.mif - | mrcalc  - dwi_brainmask_eroded.mif -mult dwi_wmMask.mif
mrthreshold  -abs 0.1 dwi_fa.mif - | mrcalc  - dwi_brainmask_eroded.mif -mult dwi_wmMask_dilated.mif

#CSD
dwi2response tournier dwi_dwi.mif wm_response.txt
dwi2fod csd dwi_dwi.mif wm_response.txt dwi_wmCsd_autolmax.mif -mask dwi_wmMask_dilated.mif -grad dwi.b -force


