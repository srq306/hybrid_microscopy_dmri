#!/bin/bash

# Generate tractography from Hybrid Orientations

export MRDIR=/opt/fmrib/mrtrix3.15/
folder=$1

${MRDIR}/bin/tckgen -algo iFOD2 -seed_image ${folder}/dwi_wmMask.nii.gz -mask ${folder}/dwi_wmMask_dilated.nii.gz -select 2000000 -step 0.2 -cutoff 0.05 -maxlength 120 -minlength 5 ${folder}/hybrid_SH_mrtrix_rescale.nii.gz tractogram.tck
