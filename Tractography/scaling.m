%Code to scale the hybrid FODs to the same amplitude level as the CSD FODs.

%Define the file directory
ddir='/vols/Data/km/srq306/3DPLI_b4k/res0.6mmX0.6mm/'
maskfile = '/home/fs0/srq306/scratch/3DPLI_b4k/res0.6mmX0.6mm/dwi_0.6mm_b4kspace_mask.nii.gz';
outfilecount = '/vols/Data/km/srq306/3DPLI_b4k/res0.6mmX0.6mm/hybrid_0.6mm_b4kspace_SH_mrtrix_rescale.nii.gz';
ratiofile = '/vols/Data/km/srq306/3DPLI_b4k/res0.6mmX0.6mm/ratio.nii.gz';
mrtrixflag=true;
vectors = load('/home/fs0/srq306/scratch/scripts/3DPLI/dir256');

if mrtrixflag
    vectors(1,:) = -vectors(1,:);
    disp('LR flip for mrtrix')
end

if ~exist([ddir 'dwi_0.6mm_b4kspace_count.nii.gz'],'file')
    disp('Rescaling hybrid orientations')
    hybrid_count=double(read_avw([ddir '/dwi_0.6mm_b4kspace_count.nii.gz']));
    hybrid_count=reshape(hybrid_count,[],256);

    SHmat = SH_transform(vectors,8);
    SHmat_pinv = pinv(SHmat);
    Ncoeffs = size(SHmat,2);        % Number of SH coeffs

    %% Load the scaling factor
    scaling_factor=read_avw(ratiofile).*mask;
    scaling_factor=reshape(scaling_factor,[],1);

    %%Scale the FOD
    hybrid_countn = hybrid_count./sum(hybrid_count,2);
    hybrid_countn(isnan(hybrid_countn)) = 0;
    mask = double(read_avw(maskfile));
    [s1,s2,s3] = size(mask);

    ind=1:size(hybrid_count,1);
    count_scale(ind,:)=hybrid_countn(ind,:).*scaling_factor(ind,:);
    count_scale(isnan(count_scale)) = 0;
    coeffs_scale = SHmat_pinv*count_scale';
    SH_3D_scale = reshape(coeffs_scale',s1,s2,s3,Ncoeffs);

    %% save the rescaled FOD
    zres=0.6;xres=0.6;yres=0.6;
    save_avw(SH_3D_scale,outfilecount,'f',[xres,yres,zres,1]);
    system(['fslcpgeom ' maskfile ' ' outfilecount ' -d']);

end
