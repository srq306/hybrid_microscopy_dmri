# Tractography
Here we provide tractography pipelines that demonstrate how to run hybrid tractography and recreate the analyses in the paper. Note: these scripts can be run both using the hybrid FODs as inputs, or CSD FODs as inputs.

**1. Bottleneck.sh**
Run the tractography to tackle the bottleneck problem

**2. Masks**
We provide the masks on the motor cortex for the investigation of the bottleneck problem. Masks were manually drawn on the motor cortex surface, converted into volume, and warped into BigMac dMRI space. Masks represent the truck, arm and face region (labeled as seg1, seg2 and seg3 for both left and right hemispheres) on the motor cortex which were used in the bottleneck problem.

**3. connectivity_tracer.m**
Generating the connectivity matrix using FODs which can be compared to the tracer. The masks for the connectivity matrix are openly available on the Brain Analysis Library of Spatial maps and Atlases (BALSA) database.

**4. tractography.sh**
Run the tractography to reconstruct the whole brain tractogram

**5. tract_segmentation.sh**
Run the white matter tract segmentation with the XTRACT atlas

**6. probability_map.py**
Refine the tracts with the XTRACT probability map

**7. cleaning.py**
Refine the tracts by removing the streamlines that were 4 standard deviations longer than the mean fibre length or deviated over 5 standard deviations relative to the core of the fibre

**8. scaling.m**
Script used to rescale the hybrid FODs to the amplitude comparable to the dMRI CSD FODs


**Software installation:**
Tractography was performed using MRtrix3 (https://www.mrtrix.org/). Fibre bundle segmentation was performed with the inclusion and exclusion masks from XTRACT to extract 42 tracts spanning the whole brain (https://fsl.fmrib.ox.ac.uk/fsl/fslwiki/XTRACT). The AFQ scripts used to optimize the streamlines can be downloaded from https://yeatmanlab.github.io/pyAFQ/installation_guide.html.
