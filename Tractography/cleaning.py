#!/usr/bin/env fslpython

"""

Cleaning the fibre tracts with AFQ, removing the streamlines which are too long or away from the tract core.


"""

import os
from dipy.io.streamline import save_tractogram, load_tractogram
import nibabel as nib
from dipy.io.stateful_tractogram import StatefulTractogram
import AFQ.segmentation as seg
from dipy.io.stateful_tractogram import Space

img = nib.load("data.nii.gz")
#define the tracts need to be cleaned
list_fiber=['ac','af_r','af_l','ar_l','ar_r','ar_l','atr_l','atr_r','cbd_r','cbd_l','cbp_r','cbp_l','cbt_l','cbt_r','cst_l','cst_r','fma','fmi','fx_l','fx_r','fa_l','fa_r','ifo_l','ifo_r','ilf_l','ilf_r','mcp','mdlf_l','mdlf_r','or_l','or_r','slf1_l','slf1_r','slf2_l','slf2_r','slf3_l','slf3_r','str_l','str_r','uf_l','uf_r','vof_l','vof_r']

for fiber in list_fiber:
    file_path=os.path.join("/vols/Data/km/srq306/hybrid_PLI/ROIs/",fiber)
    os.chdir(file_path)

    #load the tract tck file 
    fiber_file_name=fiber + '_prob.tck'
    sft = load_tractogram(fiber_file_name,img,to_space=Space.VOX)

    #clean the tract
    new_fibers, idx_in_bundle = seg.clean_bundle(sft,n_points=100, clean_rounds=3, distance_threshold=3, length_threshold=4, min_sl=20, stat='mean', return_idx=True)
    tractogram = StatefulTractogram(new_fibers.streamlines,img,Space.VOX)
    tractogram.to_rasmm()

    #save out the cleaned tract
    new_name=fiber + '_prob_clean.tck'
    save_tractogram(tractogram, os.path.join(file_path, new_name),bbox_valid_check=False)
