#!/bin/bash

#Segment the tracts with the XTRACT seed, exclude, target masks

export MRDIR=/opt/fmrib/mrtrix3.15/
folder=$1
atlas=$2


for fiber in `cat /home/fs0/srq306/scratch/scripts/fibers.txt`
do

echo "Segmenting $fiber"
mkdir ${folder}/${fiber}
cd ${folder}/${fiber}


if [ -f ${atlas}/${fiber}_stop_bin.nii.gz ]; then
    ${MRDIR}/bin/tckedit -include ${atlas}/${fiber}_seed_bin.nii.gz  -include ${atlas}/${fiber}_target_bin.nii.gz -exclude ${atlas}/${fiber}_exclude_bin.nii.gz -exclude ${atlas}/${fiber}_stop_individual_bin.nii.gz ${folder}/tractogram.tck ${fiber}.tck
else
    ${MRDIR}/bin/tckedit -include ${atlas}/${fiber}_seed_bin.nii.gz  -include ${atlas}/${fiber}_target_bin.nii.gz -exclude ${atlas}/${fiber}_exclude_bin.nii.gz ${folder}/tractogram.tck ${fiber}.tck
fi

done
