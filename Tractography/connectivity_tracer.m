% MATLAB script to create the connectivity matrix with CSD FODs or hybrid FODs
% The cortical atlas is openly available on the Brain Analysis Library of Spatial maps and Atlases (BALSA) databaset.

clc
clear all
cd /vols/Data/km/srq306/connectivity_matrix/TracerConnectivity/postmortem_diff
connectivity=zeros(29,92);
for i=1:29
    for j=2:92

        roi1=num2str(i,'%03d');
        roi2 = num2str(j,'%03d');

        command1=['tckgen -seed_random_per_voxel grot_29_L_' roi1 '_bin.nii.gz 20 -cutoff 0.03 -include grot_91_L_' roi2 '_bin.nii.gz -algorithm iFOD2 hybrid_mrtrix_FOD.nii.gz  tracts_hybrid.tck -force'];

 
        system(command1)
        
        command2 = ['tckinfo tracts_hybrid.tck | grep " count" | cut -d ":" -f 2 | tr -d [:space:]' ];
        [~,streamline_num]=system(command2);
        connectivity(i,j)=str2num(streamline_num);
        


    end

end

save('connectivity_hybrid.mat','connectivity')