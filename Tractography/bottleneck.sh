#!/bin/bash

#Script for the investigation of the bottleneck problem

folder=$1
atlas=$2
res=$3


cd ${folder}/res${res}X${res}

hemisphere="right"
segment=`seq 1 3`

for hemi in ${hemisphere}
do
    for num in ${segment}
    do
      
        tckgen -algo iFOD2  -seed_random_per_voxel ${atlas}/res${res}X${res}/motor_seg${num}_${hemi}.nii.gz 10 -step 0.2 -cutoff 0.02 -maxlength 120 -minlength 5   PLI_whole_mrtrix.nii.gz tractogram_seg${num}_${hemi}.tck
        tckedit -include ${atlas}/res${res}X${res}/internal_capsule_dwi_${res}_${hemi}.nii.gz tractogram_seg${num}_${hemi}.tck motor_seg${num}_IC_${hemi}_tract.tck
        tckmap -template ${atlas}/res${res}X${res}/fa.nii.gz -image ${atlas}/res${res}X${res}/fa.nii.gz -contrast scalar_map_count motor_seg${num}_IC_${hemi}_tract.tck motor_seg${num}_IC_${hemi}_count.nii.gz
        mrcalc motor_seg${num}_IC_${hemi}_count.nii.gz ${atlas}/res${res}X${res}/internal_capsule_dwi_${res}_${hemi}.nii.gz --multi motor_seg${num}_${hemi}_count_ICdistribute.nii.gz
        tckmap -template ${atlas}/res${res}X${res}/fa.nii.gz  motor_seg${num}_IC_${hemi}_tract.tck - | mrcalc - $(tckinfo motor_seg${num}_IC_${hemi}_tract.tck | grep " count" | cut -d ':' -f2 | tr -d '[:space:]') -div motor_seg${num}_IC_${hemi}_density.nii.gz
        mrcalc motor_seg${num}_IC_${hemi}_density.nii.gz ${atlas}/res${res}X${res}/internal_capsule_dwi_${res}_${hemi}.nii.gz --multi motor_seg${num}_${hemi}_density_ICdistribute.nii.gz
    done
    
find_the_biggest motor_*_${hemi}_count_ICdistribute.nii.gz motor_${hemi}_count_IC_distribution.nii.gz
find_the_biggest motor_*_${hemi}_density_ICdistribute.nii.gz motor_${hemi}_density_IC_distribution.nii.gz
done
