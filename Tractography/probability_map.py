#!/usr/bin/env fslpython
"""

Optimize the tracts with the xtract probability map

"""

import os
import nibabel as nib
from dipy.io.streamline import save_tractogram, load_tractogram
import numpy as np
from dipy.io.stateful_tractogram import Space
import dipy.tracking.streamline as dts
import dipy.tracking.streamlinespeed as dps

#Define the tracts need to be optimized
list_fiber=['ac','af_r','af_l','ar_l','ar_r','ar_l','atr_l','atr_r','cbd_r','cbd_l','cbp_r','cbp_l','cbt_l','cbt_r','cst_l','cst_r','fma','fmi','fx_l','fx_r','fa_l','fa_r','ifo_l','ifo_r','ilf_l','ilf_r','mcp','mdlf_l','mdlf_r','or_l','or_r','slf1_l','slf1_r','slf2_l','slf2_r','slf3_l','slf3_r','str_l','str_r','uf_l','uf_r','vof_l','vof_r']

for fiber in list_fiber:
    
    img = nib.load("data.nii.gz")
    
    file_path=os.path.join("/vols/Data/km/srq306/hybrid_PLI/ROIs/",fiber)
    os.chdir(file_path)

    #optimization probability threshold in %
    prob_threshold=20

    #load the tract tck file
    fiber_file_name=fiber + '.tck'
    tg = load_tractogram(fiber_file_name,img, to_space=Space.VOX)

    #load the probability map available in FSL XTRACT
    pro_file_name='xtract_prob_' + fiber + '_registrated.nii.gz'
    prob_img=nib.load(op.join("/home/fs0/srq306/scratch/atlas/probability_map/res0.6mmX0.6mm/",pro_file_name))
    prob_map=prob_img.get_fdata() 

    #calculate probability value for each streamline
    streamlines = tg.streamlines
    fgarray=np.array(dps.set_number_of_points(streamlines, 100)) 
    n_streamlines = fgarray.shape[0] 
    fiber_probabilities = dts.values_from_volume(prob_map,fgarray, np.eye(4)) 
    fiber_probabilities = np.mean(fiber_probabilities, -1) 
    idx_above_prob = np.where(fiber_probabilities > prob_threshold) 
    sl_idxs = idx_above_prob[0]
    sl=[]

    #select streamline with probability higher than the threshold
    for sl_idx in sl_idxs: 
        if fiber_probabilities[sl_idx] > prob_threshold:
            sl.append(tg.streamlines[sl_idx])     
        else: 
            continue
    
    #save out the probability thresholded tract
    tg.streamlines=sl
    new_name=fiber + '_prob.tck'
    save_tractogram(tg, os.path.join(file_path, new_name),bbox_valid_check=False)
