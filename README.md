# Hybrid dMRI-microscopy tractography
This repository presents a novel approach that combines diffusion MRI and microscopy for 3D fibre orientations reconstruction.

**Step-by-step Tutorial:**
Explore our [tutorial](https://open.win.ox.ac.uk/pages/srq306/hybridtractographydoc/) for a detailed explanation of the code. The tutorial covers running tractography with hybrid FODs using simple command lines and generating hybrid FODs with a step-by-step instructions.

**Getting Started:**
**Installtion**
Please download the repository using git clone. 
`git clone https://git.fmrib.ox.ac.uk/srq306/hybrid_microscopy_dmri.git`

The repository includes code for generating hybrid orientations and running tractography, as well as recreating the figures in the paper. Run times for downloading are typically less than several minutes, and no non-standard hardware is required. GPUs can be beneficial for running tractography on large microscopy data


**Data Availability:**
The BigMac dataset is openly available on the [Digital brain bank](https://open.win.ox.ac.uk/DigitalBrainBank/#/datasets/anatomist).
Hybrid FODs will also be made openly available upon publication.

**System requirements:**
This code has been tested on a Mac using MATLAB 2019a-2021a and Python 3. MRI-microscopy co-registration was performed using TIRL V3.1.1, part of FSL, available at https://git.fmrib.ox.ac.uk/ihuszar/tirl. MRI data were analyzed using the FMRIB Software Library (FSL 6.0.5/6.0.6), available at https://fsl.fmrib.ox.ac.uk/fsl/fslwiki, and MRtrix3 software, available at https://www.mrtrix.org/.


**Expected input:**
1. Diffusion MRI fibre orientation eg. ball and stick model output.
2. Microscopy fibre orientation registered to the diffusion MRI space. In the demo, we provided a single slide of PLI data. After registration with the TIRL software library, the fibre orientation is in tif format with orientations (inplane and norm) and pixel coordinates in diffusion MRI space 
3. Specify whether the processing is for the whole microscopy slice or a small region.

**Demo:**
We provide the example data for the reviewers. The demostration code is provided as ~/scripts/HybridOrientation/demo.m with a detailed explanation of how to run the hybrid orientation.

**Expected output:**
The hybrid Fibre Orientation Distributions (FODs) in each voxel in the dMRI space. Output is in spherical harmonics format and can be visualized with MRview or FSLeyes. Three resolutions are provided: 1mm, 0.6mm, and 0.4mm.

The code may take some time to run. We provide the output in the following directory:
~/hybrid/0.4mm/*
~/hybrid/0.6mm/*
~/hybrid/1mm/*

**Expected processing time:**
For a small block of the region, only a few minutes (~10min) are needed. But if you would like to run for the whole microscopy slice, a few hours (~3h) may be required.

**Folder content**:

*1. Hybrid Orientation*

Here we provide the code and Matlab functions to generate the hybrid orientations. The step-by-step instruction on how to run the code is provided as demo.m for a single slide of PLI data. Note: you may want to change the directory to where you store the downloaded data. The explanation of each function is included as a readme file in this folder.

*2. Tractography*

Here we provide tractography pipelines that demonstrate how to run hybrid tractography and recreate the analyses in the paper, including whole brain tractography, tract segmentation, investigation of the bottleneck problem, and generating brain connectivity matrix. Note, these scripts can be run both using the hybrid FODs as inputs, or CSD FODs as inputs, facilitating meaningful comparisons between hybrid tractography outputs and those from dMRI-only based analyses.

*3. CSD*

We provide the script for running the CSD FODs on the diffusion MRI data. The output FODs can be used for tractography.

**Citation:**
Preprint: Silei Zhu et al., Imaging the structural connectome with hybrid diffusion MRI-microscopy tractography, https://www.biorxiv.org/content/10.1101/2024.01.08.574641v1

**Contact me:**
If you have any questions, please feel free to contact silei.zhu@ndcn.ox.ac.uk

