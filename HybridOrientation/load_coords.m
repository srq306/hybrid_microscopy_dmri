function c = load_coords(file,mri)
    %Load microscopy coordinates from the TIRL output
    %More information about the TIRL can be found https://git.fmrib.ox.ac.uk/ihuszar/tirl

    %file:      -coordinate file directory and file name
    %mri:       -MRI mask file

    x = double(imread([file '_x.tif']));
    [s1,s2,s3] = size(mri);

    c = nan(size(x,1),size(x,2),3);
    c(:,:,1) = x./255.*s1;

    clear x
    c(:,:,2) = double(imread([file '_y.tif']))./255.*s2;
    c(:,:,3) = double(imread([file '_z.tif']))./255.*s3;

    % Change from 0 indexing to 1 indexing
    c = c+1;

end
