function out = hist_sphere(micro,vect,SHmat_pinv)

    % find nearest vector for each hybrid orientation
    % and generate frequency histogram of orientations along each vect

    %micro:     -3D vector of hybrid orientations 
    %vect:      -vectors evenly distributed across a sphere
    %SHmat_pinv -optional, if provided the spherical harmonics basis, fit the spherical harmonics
    
    cosangsqrd = nan(size(micro,1),size(vect,2));
    for s = 1:size(vect,2)
        cosangsqrd(:,s) = (micro*vect(:,s)).^2;
    end
    [~,ind] = max(cosangsqrd,[],2);
    count = hist(ind,1:size(vect,2));

    out.count = count;

    % normalise
    countn = count./sum(count);

    % fit spherical harmonics
    if exist('SHmat_pinv','var')
        out.coeffs = SHmat_pinv*countn';
    end

end
