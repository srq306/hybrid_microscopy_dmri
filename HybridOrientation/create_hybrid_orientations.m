function create_hybrid_orientations(slideno,microfolder,dwifolder,maskfile,dwiout,debugging)
    %The main function used to generate the hybrid FOD

    %slideno:       -the number of microscopy slices
    %microfolder:   -the directory of microscopy data in the diffusion mri space (we used the TIRL registration output)
    %dwifolder     -directory of dMRI folder including BAS output (data.bedpostX) and DTI output (data.dtifit)
    %maskfile       -directory of the brain mask file
    %dwiout         -output file name
    %debugging      -if true, will only calculate the hybrid orietnation of a small region of interest


    if ~exist('dwifolder','var')
        disp("Directory of the diffusion data need to be provided")    
    else
        bdpxfolder = [dwifolder '/data.bedpostX'];
    end

    if ~exist('dwiout','var')
        dwiout  = 'dwi_0.6mm_b4kspace';
    end

    if ~exist('microfolder','var')
        disp("Directory of the microscopy data need to be provided")
    else
        base = microfolder;
    end

    if ~debugging
        ddir = [base '/HybridOrientations/'];
    else
        ddir = [base '/HybridOrientations_roi/'];
    end

        if ~exist([ddir '/' dwiout '_hybrid_orientations.mat'],'file')
            disp('Generating hybrid orientations')

            if ~exist('dyads','var')

                % Load dyads(+ dispersion) output from diffusion MRI ball and stick model
                disp('Loading BAS data')
                mask = double(read_avw(maskfile));
                mask(mask==0) = nan;

                dyads.th1 = double(read_avw([bdpxfolder '/merged_th1samples.nii.gz'])).*mask;
                dyads.th2 = double(read_avw([bdpxfolder '/merged_th2samples.nii.gz'])).*mask;
                dyads.th3 = double(read_avw([bdpxfolder '/merged_th3samples.nii.gz'])).*mask;
                dyads.phi1 = double(read_avw([bdpxfolder '/merged_ph1samples.nii.gz'])).*mask;
                dyads.phi2 = double(read_avw([bdpxfolder '/merged_ph2samples.nii.gz'])).*mask;
                dyads.phi3 = double(read_avw([bdpxfolder '/merged_ph3samples.nii.gz'])).*mask;

                % load volume fraction masks to mask dyads which dont survive the threshold
                dyads.f2 = double(read_avw([bdpxfolder '/mean_f2samples.nii.gz'])).*mask;
                dyads.f3 = double(read_avw([bdpxfolder '/mean_f3samples.nii.gz'])).*mask;
                thresh = 0.05
                dyads.f2(dyads.f2<thresh) = nan; dyads.f2(dyads.f2>=thresh) = 1;
                dyads.f3(dyads.f3<thresh) = nan; dyads.f3(dyads.f3>=thresh) = 1;
            end

            main(ddir,dwiout,dyads,mask,debugging);
        end


end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function main(ddir,dwiout,dyads,mask,debugging)

    %load the microscopy coordinate, inplane angle and normal from tirl output
    finplane = [dwiout  '_inplane'];
    fnorm = [dwiout '_norm'];
    fcoords = [dwiout '_coords'];

    if ~exist(ddir,'dir')
        mkdir(ddir);
    end
        base=[ddir,'/../']
        micro.inplane = load_vector([base finplane]);
        micro.norm = load_vector([base fnorm]);

        [micro.hr1,micro.hr2,~] = size(micro.inplane);
        micro.coords = load_coords([base fcoords],mask);


    disp('micro data loaded')

    % Remove (=nan) all micro.coords which do not fit in the tissue mask
    tmp = reshape(micro.coords,[],3);
    ind = tmp(:,1)<1|tmp(:,2)<1|tmp(:,3)<1;
    tmp(ind,:) = nan;
    ind = tmp(:,1)>size(mask,1)|tmp(:,2)>size(mask,2)|tmp(:,3)>size(mask,3);
    tmp(ind,:) = nan;
    i = sub2ind(size(mask),floor(tmp(:,1)),floor(tmp(:,2)),floor(tmp(:,3)));
    i(isnan(i)) = [];
    tmp(isnan(mask(i)),:) = nan;
    tmp = reshape(tmp,size(micro.coords));
    micro.coords = tmp;
    clear tmp

    if debugging
        % extract ROI for testing, top left hand corner
        r1 = 1:ceil(micro.hr1/2);
        r2 = 1:ceil(micro.hr2/2);

        micro.inplane = micro.inplane(r1,r2,:);
        micro.norm = micro.norm(r1,r2,:);
        micro.coords = micro.coords(r1,r2,:);
    end


    %% PROJECTION %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % compare micro inplane with BAS samples projected onto the microscopy plane
    % output BAS orientation closest to the micro pixel
    % i.e. correct the inclination
    projected = compare_projections(micro,dyads,1*10^4);
    projected.pop = ceil(projected.ind./size(dyads.th1,4));

    %If a2(BAS projected on to the plane) is more than pi/2 away from inplane, must negate direction
    clear tmp
    tmp.inplane = reshape(micro.inplane,[],3);
    tmp.a1 = reshape(projected.a1,[],3);
    tmp.a2 = reshape(projected.a2,[],3);
    cosang = sum(tmp.inplane.*tmp.a2,2);

    tmp.a1(cosang<0,:) = -tmp.a1(cosang<0,:);
    tmp.a2(cosang<0,:) = -tmp.a2(cosang<0,:);


    % Calulate a2 norm - more fair visual comparison with micro.inplane
    tmp.a2norm = tmp.a2./vecnorm(tmp.a2,2,2);
    projected.a1 = reshape(tmp.a1,size(projected.a1));
    projected.a2 = reshape(tmp.a2,size(projected.a2));
    projected.a2norm = reshape(tmp.a2norm,size(projected.a2));

    % Calculate the hybrid 3D orientation 
    micro.vect3D = tmp.inplane.*vecnorm(tmp.a2,2,2)+tmp.a1;
    micro.vect3D = reshape(micro.vect3D,size(micro.inplane));

    %save out the hybrid orientations and intermediate orientation
    fields = {'inplane','norm','hr1','hr2'};
    micro = rmfield(micro,fields);
    projected = rmfield(projected,'a2norm');
    save([ddir dwiout '_hybrid_orientations.mat'],'projected','micro','-v7.3')

    disp('Processing complete')


end
