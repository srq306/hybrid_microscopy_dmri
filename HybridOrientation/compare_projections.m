function out = compare_projections(micro,dyads,batchsize)
    %Project the dyads to the microscopy plane and compare it with the 2D microscopy orientations
    %micro:      - include microscopy coordinates and microscopy inplane angle
    %dyads:      - dMRI BAS dyads
    %batchsize:  - process the comparison in batches to prevent memory issue. eg.1*10^4

    micro.coords = floor(micro.coords);
    [s1,s2,~] = size(micro.inplane);

    out.a1 = nan(s1*s2,3);
    out.a2 = nan(s1*s2,3);
    out.ang = nan(s1*s2,1);
    out.ind = nan(s1*s2,1);

    tmpcoords = reshape(micro.coords,[],3);
    tmpinplane = reshape(micro.inplane,[],3);
    tmpnorm = reshape(micro.norm,[],3);

    % Only process pixels inside tissue mask
    removed = isnan(tmpcoords(:,1));
    tmpcoords(removed,:) = [];
    tmpnorm(removed,:) = [];
    tmpinplane(removed,:) = [];

    a1 = nan(size(tmpcoords));
    a2 = nan(size(tmpcoords));
    ang = nan(size(tmpcoords,1),1);
    ind = nan(size(tmpcoords,1),1);

    % linearise dyads
    dyads.s = size(dyads.th1);
    dyads.th1 = reshape(dyads.th1,[],dyads.s(4));
    dyads.th2 = reshape(dyads.th2,[],dyads.s(4));
    dyads.th3 = reshape(dyads.th3,[],dyads.s(4));
    dyads.phi1 = reshape(dyads.phi1,[],dyads.s(4));
    dyads.phi2 = reshape(dyads.phi2,[],dyads.s(4));
    dyads.phi3 = reshape(dyads.phi3,[],dyads.s(4));

    % project dyads on to the microscopy plane
    for start = 1:batchsize:size(tmpcoords,1)
        stop = min([start+batchsize-1,size(tmpcoords,1)]);

        i = tmpcoords(start:stop,1);
        j = tmpcoords(start:stop,2);
        k = tmpcoords(start:stop,3);

        indd = sub2ind(size(dyads.f2),i,j,k);

        selected = dyadprojection(tmpinplane(start:stop,:),tmpnorm(start:stop,:),...
            dyads,indd);
        a1(start:stop,:) = selected.a1;
        a2(start:stop,:) = selected.a2;
        ang(start:stop) = selected.ang;
        ind(start:stop) = selected.ind;

        disp(start)

    end

    out.a1(~removed,:) = a1;
    out.a2(~removed,:) = a2;
    out.ang(~removed) = ang;
    out.ind(~removed) = ind;

    % reshape outputs
    out.ang = reshape(out.ang,s1,s2);
    out.ind = reshape(out.ind,s1,s2);
    out.a1 = reshape(out.a1,s1,s2,3);
    out.a2 = reshape(out.a2,s1,s2,3);

end
