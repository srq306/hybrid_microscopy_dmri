function v = load_vector(file)
    %Load the vector from the TIRL output
    %More information about TIRL can be found https://git.fmrib.ox.ac.uk/ihuszar/tirl

    %file:      -vector file directory
    
    x = double(imread([file '_x.tif']));
    [s1,s2] = size(x);
    v = nan(s1,s2,3);
    v(:,:,1) = x;
    clear x
    v(:,:,2) = double(imread([file '_y.tif']));
    v(:,:,3) = double(imread([file '_z.tif']));

    % Set range -1 - 1
    v = v/255;
    v = v*2-1;


end

