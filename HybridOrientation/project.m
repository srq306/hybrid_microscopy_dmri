function out = project(v,n)
    %project the vector on to the normal and onto the plane

    %v:     -dMRI dyads vector in x,y,z coordinates
    %n:     -microscopy normal

    % Ensure norm is unit
    n = n./vecnorm(n,2,2);

    % Find vector projection
    % i.e. the vector projected onto the normal
    % https://en.wikipedia.org/wiki/Vector_projection
    a1 = sum(v.*n,2).*n;

    % i.e. the vector projected onto the plane
    a2 = v-a1;

    out.a1 = a1;
    out.a2 = a2;

end
