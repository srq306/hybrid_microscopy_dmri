function selected = dyadprojection(microvect,micronorm,dyads,ind)
    %project the dMRI BAS dyads to the microscopy plane and microscopy norm

    %microvect:         -microscopy inplane angle
    %micronorm:         -microscopy normal
    %dyads:             -dMRI BAS dyads
    %ind:               -index of batch
    
    % Concatenate all dyad populations together
    dyads.f2(dyads.f2==0) = nan;
    dyads.f3(dyads.f3==0) = nan;

    th = nan(numel(ind),3*size(dyads.th1,2));
    phi = nan(numel(ind),3*size(dyads.th1,2));
    th(~isnan(ind),:) = [dyads.th1(ind(~isnan(ind)),:),dyads.th2(ind(~isnan(ind)),:)...
        .*dyads.f2(ind(~isnan(ind))),dyads.th3(ind(~isnan(ind)),:)...
        .*dyads.f3(ind(~isnan(ind)))];
    phi(~isnan(ind),:) = [dyads.phi1(ind(~isnan(ind)),:),dyads.phi2(ind(~isnan(ind)),:)...
        .*dyads.f2(ind(~isnan(ind))),dyads.phi3(ind(~isnan(ind)),:)...
        .*dyads.f3(ind(~isnan(ind)))];

    npixels = numel(ind);
    nsamples = size(th,2);

    % Project the dyads onto the 2D microscopy plane
    % a2 is the dyad angle on the microscopy plane
    % a1 is the dyad angle projected on to the microscopy norm
    a1 = nan(npixels,nsamples,3);
    a2 = nan(npixels,nsamples,3);
    cosangsqrd = nan(npixels,nsamples);

    for s = 1:nsamples

            out = projection(th(:,s),phi(:,s),micronorm);
            a1(:,s,:) = out.a1;
            a2(:,s,:) = out.a2;

            % Calc angle difference between normalised a2 and microvect
            a2norm = out.a2./vecnorm(out.a2,2,2);
            cosangsqrd(:,s) = (sum(microvect.*a2norm,2)).^2;

    end

    % Record smallest angle between vector and any dyad sample
    [~,indd] = max(cosangsqrd,[],2,'omitnan');
    linearind = sub2ind(size(cosangsqrd),1:size(cosangsqrd,1),indd'); %'
    angg = acos(sqrt(cosangsqrd(linearind)));


    a1 = reshape(a1,[],3);
    a2 = reshape(a2,[],3);

    % Output dyad sample most closely associated with each micro orientation
    selected.a1 = a1(linearind,:);
    selected.a2 = a2(linearind,:);
    selected.ang = angg;
    selected.ind = indd;

end
