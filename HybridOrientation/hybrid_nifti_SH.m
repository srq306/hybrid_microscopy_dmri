function hybrid_nifti_SH(base,maskfile,xres,yres,bdpxfolder,dwiout,debugging,mrtrixflag)
    % Fit the hybrid fibre orientation distribution to the spherical harmonics

    %base:      -output file directory
    %maskfile:  -mask file directory
    %xres:      -inplane resolution
    %yres:      -through plane resolution
    %bdpxfolder:-dMRI BAS output
    %dwiout:    -output file name
    %debugging: -if debugging, only process a small FOV of slice
    %mrtrixflag:-if True, save the output spherical harmonics in mrtrix format (flip the x axis)

    zres = xres;    % inplane resolution, yres=through plane resolution
    mask = double(read_avw(maskfile));

    if ~debugging
        base = [base '/HybridOrientations/'];
    else
        base = [base '/HybridOrientations_roi/'];
    end

    if ~exist('mrtrixflag','var')
        mrtrixflag = false;
    end

    if mrtrixflag
        flag = '_mrtrix';
    else
        flag = '_fsl';
    end

    % Specify outputs directory and file name

    outbase = [base '/res' num2str(xres) 'mmX' num2str(yres) 'mm/'];

    if ~exist(outbase,'dir')
        mkdir(outbase)
    end

    header_file = maskfile;
    outfile = [outbase dwiout '_SH' flag '.nii.gz'];
    outfilecount = [outbase dwiout '_count.nii.gz'];
    outfilemask = [outbase dwiout '_mask.nii.gz'];
    outfilemasknative = [outbase dwiout '_mask_native.nii.gz'];
    outfilefa = [outbase dwiout '_fa.nii.gz'];

    % If debugging, only process a small ROI, if not debugging, process the whole slice
    roimask = zeros(size(mask));
    [s1,s2,s3] = size(mask);
    
    xextent = 1:s1;
    yextent = 1:s2;
    zextent = 1:s3;

    if debugging
        xroi = 65:100;
        yroi = 73:80;
        zroi = 70:90;
        roimask(xroi,yroi,zroi) = 1;
        disp('Debugging - Processing roi')
    else
        roimask(xextent,yextent,zextent) = 1;
    end
    
    roimask = roimask.*mask;

    % Define the sphere sampling the spherical harmonics
    vectors = load('dir256')';

    % Check if count.nii.gz is already created
    if exist(outfilecount,'file')
        count = double(read_avw(outfilecount));
        [s1,s2,s3,s4] = size(count);
        count = reshape(count,[],256);
    else

        % Create nifti matrix at specified resolution
        % Current matrix volume
        xmm = numel(xextent);
        ymm = numel(yextent);
        zmm = numel(zextent);

        % New matrix size according to the defined resolutions
        s1 = ceil(xmm/xres*0.6);
        s2 = ceil(ymm/yres*0.6);
        s3 = ceil(zmm/zres*0.6);

        % Number of hr voxels in mask
        Nvox = s1*s2*s3;        

        % Create empty histogram to store count of pixels which match each direction
        count = zeros(Nvox,size(vectors,2));

        % upsample mask and save
        roimask_us = imresize3(roimask(xextent,yextent,zextent),[s1,s2,s3],'nearest');

        if ~exist(outfilemask,'file')
            save_avw(roimask_us,outfilemask,'f',[xres,yres,zres,1]);
            system(['fslcpgeom ' header_file ' ' outfilemask ' -d']);
        end

        if ~exist(outfilemasknative,'file')
            % Save mask in native space
            save_avw(roimask,outfilemasknative,'f',[1,1,1,1]);
            system(['fslcpgeom ' header_file ' ' outfilemasknative ' -d']);
        end

        if ~exist(outfilefa,'file')
            % Also upsampled FA mask
            fa = double(read_avw([bdpxfolder '/../data.dtifit/dti_FA.nii.gz']));
            fa_us = imresize3(fa(xextent,yextent,zextent),[s1,s2,s3]);
            save_avw(fa_us,outfilefa,'f',[xres,yres,zres,1]);
            system(['fslcpgeom ' header_file ' ' outfilefa ' -d']);
        end

        % Loop through files - now changed to output a single file per slices
        microfile = [outbase '/../' dwiout '_hybrid_orientations.mat'];

        % Load micro info for the file
        tmp = load(microfile,'micro');
        micro = tmp.micro;

        % Mask Left/Right tissue
        im = double(imread([outbase '/../../' dwiout '_norm_y.tif']));
        im = im/255*2-1;
        micro_mask = double(im~=0);

        if debugging
            [m1,m2] = size(im);
            r1 = 1:ceil(m1/2);
            r2 = 1:ceil(m2/2);
            micro_mask = micro_mask(r1,r2);
        end

        micro.vect3D = micro.vect3D.*micro_mask;
        micro.coords = micro.coords.*micro_mask;
        micro.vect3D = reshape(micro.vect3D,[],3);

        % Convert coords to new resolution
        coordshr = micro.coords;

        % Want HR voxel coords to start at 1
        coordshr(:,:,1) = coordshr(:,:,1)-(xextent(1)-1);
        coordshr(:,:,2) = coordshr(:,:,2)-(yextent(1)-1);
        coordshr(:,:,3) = coordshr(:,:,3)-(zextent(1)-1);
        coordshr(:,:,1) = coordshr(:,:,1)./xres*0.6;
        coordshr(:,:,2) = coordshr(:,:,2)./yres*0.6;
        coordshr(:,:,3) = coordshr(:,:,3)./zres*0.6;
        coordshr = floor(coordshr);

        % Linearise
        tmp = reshape(coordshr,[],3);
        % Remove coords outside of mask
        tmp(tmp(:,1)<1,:) = nan;
        tmp(tmp(:,2)<1,:) = nan;
        tmp(tmp(:,3)<1,:) = nan;
        tmp(tmp(:,1)>s1,:) = nan;
        tmp(tmp(:,2)>s2,:) = nan;
        tmp(tmp(:,3)>s3,:) = nan;

        % Find index related to coordinate
        voxind = sub2ind([s1,s2,s3],tmp(:,1),tmp(:,2),tmp(:,3));
        clear tmp

        % For each voxel in hr space, extract fibre orientations, compare
        % to directions in 'vectors' and populate frequency histogram
        VV = unique(voxind);
        VV(isnan(VV)) = [];
        for w = 1:numel(VV)
            v = VV(w);
            if roimask_us(v)==1
                ind = voxind==v;
                if sum(ind)>0
                    out = hist_sphere(micro.vect3D(ind,:),vectors);
                    count(v,:) = count(v,:)+out.count;
                end
            end
            if mod(w,500)==0, disp([num2str(w) '/' num2str(numel(VV))]); end
        end
    end

    % Fit SH coeffs to frequency matrix
    if mrtrixflag
        vectors(1,:) = -vectors(1,:);
        disp('LR flip for mrtrix')
    end
    SHmat = SH_transform(vectors,8); % get the spherical harmonics basis
    SHmat_pinv = pinv(SHmat);
    Ncoeffs = size(SHmat,2);       

    % Normalise hsitogram by number of pixels in each voxel
    countn = count./sum(count,2);
    coeffs = SHmat_pinv*countn';
    SH_3D = reshape(coeffs',s1,s2,s3,Ncoeffs);
    count_3D = reshape(count,s1,s2,s3,256);

    %save the output
    if ~exist(outfilecount,'file')
        save_avw(count_3D,outfilecount,'f',[xres,yres,zres,1]);
        system(['fslcpgeom ' header_file ' ' outfilecount ' -d']);
    end

    save_avw(SH_3D,outfile,'f',[xres,yres,zres,1]);
    system(['fslcpgeom ' header_file ' ' outfile ' -d']); 

end
