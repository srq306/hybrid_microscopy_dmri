# Code for generating the hybrid orientation

Here we have code to create 3D hybrid fibre orientations by combining 
information from diffusion MRI and microscopy.
Explore our [tutorial](https://open.win.ox.ac.uk/pages/srq306/hybridtractographydoc/) for a detailed explanation of the code.
The code has been tested on MATLAB 2019-2021a.

**demo.m**
Demonstrates how hybrid orientations are created, here using a single slide
of microscopy data. The main functions for processing hybrid orientations should be run in 
order, as outlined below.

**create_hybrid_orientations.m**
This function takes the warped microscopy fibre orientations, and compares
them with the coregistered output from the ball and stick (BAS) model. The BAS
output is first projected onto the microscopy plane, we extract
whichever BAS orientation is most similar to that from microscopy (within 
the plane) and then approximate the through plane angle with that from 
dMRI. This is repeated for each microscopy pixel.
The function operates on each microscopy slide separately.
The function outputs a mat file that contains 3D hybrid fibre orientations
at the native resolution of the microscopy data.

**hybrid_nifti_SH.m**
Next, we combine fibre orientations across a local neighbourhood, equivalent
to some specified voxel resolution, to create a hybrid fibre orientation 
distribution (FOD) for each voxel. The hybrid fibre orientations
populate a 3D orientation histogram across the sphere, which is then converted
to spherical harmonic coefficients. Consequently, the hybrid FODs can be 
easily compared to those from dMRI (e.g. those output from constrained 
spherical deconvolution, mrtrix).
Again, this function acts on each microscopy slide separately, and outputs
a set of NIFTI volumes per slide. One contains the spherical harmonic 
coefficients. Another contains the frequency distribution (count) of 
hybrid orientations across the sphere.

**hybrid_nifti_SH_combined.m**
This function then combines information across multiple microscopy slides
to create a densely filled volume of FODs across the brain.

## Other functions and files used
The following functions and files are called during implementation

**dir256**
Describes 256 vectors which are evenly distributed across the sphere, 
which are used to define the 3D orientation histogram.

**SH_transform.m**
Code to create spherical harmonic transform
Tractography reconstruction of the corticospinal tract was then completed 
using mrtrix.

**dyadprojection.m & projection.m & project.m**
Code to project the 3D dMRI ball and stick dyads to the microscopy normal and microscopy plane.

**compare_projections.m**
Code to project the dyads to the microscopy plane and compare it with the 2D microscopy orientations.

**load_vector.m & load_coords**
Code to load the microscopy vectors and microscopy coordinates stored by TIRL.

**hist_sphere.m**
Code to find the nearest vector for each hybrid orientation and generate frequency histogram of orientations along each vector.