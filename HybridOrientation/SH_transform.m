function SH_mat = SH_transform(coords,L_max)
% Calculates spherical harmonic components
% coords:           - Coordinate basis
% L_max:            - Order of spherical harmonics (determines # coeffs)

if size(coords,1)~=3
    coords = coords';
end

[phi,th] = cart2sph(coords(1,:),coords(2,:),coords(3,:));
th = pi/2 - th;

SH_mat = [];

for l = 0:2:L_max
    m = 0:l; 
    % For eqn see https://en.wikipedia.org/wiki/Spherical_harmonics &
    % mrtrix documentation
    a1 = (2*l+1)/(4*pi);
    a2 = factorial(l-m)./factorial(l+m);
    P = legendre(l,cos(th));
    e = exp(1i*m'*phi);
    C = sqrt(a1.*a2);
    
    Yl = C'.*P.*e;
    % Note, must transpose after taking real/imag parts, otherwise
    % imaginary part becomes negative
    
    coeffs = zeros(size(coords,2),2*l+1);
    % Different equations for m<0, m=0, m>0
    coeffs(:,1:l) = sqrt(2)*imag(Yl(end:-1:2,:))';
    coeffs(:,l+1) = Yl(1,:)';
    coeffs(:,l+2:end) = sqrt(2)*real(Yl(2:end,:))';

    SH_mat = [SH_mat,coeffs];
end

end 















