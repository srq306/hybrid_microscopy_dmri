function hybrid_nifti_SH_combined(xres,yres,dwiout,mrtrixflag,files_txt,outdir,mask_file)
    % Create nifti file with hybrid SH coefficients combined from multiple microscopy slices
    
    %xres:      -inplane resolution
    %yres:      -through-plane resolution
    %dwiout:    -output file name
    %mrtrixflag:-if True, save the output spherical harmonics in mrtrix format (flip the x axis)
    %files_txt: -list of files including the directory of count.nii.gz file
    %outdir:    -output file directory
    %mask_file: -mask file directory

    % load the files_txt
    fid = fopen(files_txt);
    files = textscan(fid, '%s','delimiter','\n');
    fclose(fid);
    files = files{1};
    disp(['No slides = ' num2str(numel(files))])

    if ~exist('mrtrixflag','var')
        mrtrixflag = false;
    end

    if mrtrixflag
        flag = '_mrtrix';
    else
        flag = '_fsl';
    end

    zres = xres;

    outfilecounttot = [outdir '/' dwiout '_count.nii.gz'];
    outfiletot = [outdir '/' dwiout '_SH' flag '.nii.gz'];

    %Combine the FOD count file from multiple microscopy slices
    if exist(outfilecounttot,'file')
        count = double(read_avw(outfilecounttot));
    else

        for s = 1:numel(files)

            outfilecount = [files{s} '/res' num2str(xres) 'mmX' num2str(yres) 'mm/' dwiout '_count.nii.gz'];
            disp(outfilecount)

            if exist(outfilecount,'file')
                if s==1
                    count = double(read_avw(outfilecount));
                else
                    try
                        count = count+double(read_avw(outfilecount));
                    catch
                        disp(['missing: ' outfilecount])
                    end
                end
            else
                disp(['Missing file ' outfilecount])
            end

        end

        save_avw(count,outfilecounttot,'f',[xres,yres,zres,1]);
        system(['fslcpgeom ' mask_file ' ' outfilecounttot ' -d']);

    end

    % Fit SH coeffs to frequency matrix
    % Normalise histogram by number of pixels in each voxel

    % Define number of spherical harmonic coeffs
    vectors = load('/home/fs0/srq306/scratch/scripts/3DPLI/dir256')';

    %if mrtrix, flip the x axis
    if mrtrixflag
        vectors(1,:) = -vectors(1,:);
    end

    %calculate the spherical harmonics basis and convert count amplitude to spherical harmonics
    SHmat = SH_transform(vectors,8);
    SHmat_pinv = pinv(SHmat);
    Ncoeffs = size(SHmat,2);        % Number of SH coeffs

    [s1,s2,s3,~] = size(count);
    count = reshape(count,[],256);
    countn = count./sum(count,2);
    coeffs = SHmat_pinv*countn';
    SH_3D = reshape(coeffs',s1,s2,s3,Ncoeffs);

    %save out the file
    save_avw(SH_3D,outfiletot,'f',[xres,yres,zres,1]);
    system(['fslcpgeom ' mask_file ' ' outfiletot ' -d']);

    if ~exist([outdir '/mask.nii.gz'],'file')
        system(['cp ' mask_file ' ' outdir '/mask.nii.gz'])
    end

    disp('finished')

end
