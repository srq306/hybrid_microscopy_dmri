function out = projection(th,phi,n)
    %projec the dMRI dyads to the microscopy normal and microscopy plane

    %th:        -dMRI dyads theta
    %phi:       -dMRI dyads phi
    %n:         -microscopy normal vector
    
    % Convert dyad to x y z
    [x,y,z] = sph2cart(phi,pi/2-th,ones(size(th)));
    out = project([x,y,z],n);

end
