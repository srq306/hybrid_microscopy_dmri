%% Demo script - hybrid fibre orientations

% Create hybrid orientations from coregisterd microscopy and dMRI ball and
% stick outputs.

% Add the folder to the directory before running


%% Step 0
% Use TIRL to map microscopy pixels to MRI space
% See scripts as part of the BigMac dataset (https://git.fmrib.ox.ac.uk/amyh/bigmacanalysis/-/tree/main/TIRL) for details.

% The output is stored in ~/microscopy_PLI_P050/data/tirl_output/
% dwi_0.6mm_b4kspace_coords* are the xyz pixel coordinates in MRI space
% dwi_0.6mm_b4kspace_inplane* defines the 3D hybrid orientations in MRI space
% dwi_0.6mm_b4kspace_normal* defines normal to the microscopy plane in MRI space



%% Step 1

% For each microscopy pixel, approximate the through plane angle with that
% from diffusion MRI

% Inputs: PLI in_plane orientations in dwi space + ball and stick output
% from dwi (dwi_0.6mm_b4kspace_inplane*)
% Outputs: hybrid orientations, stored as .mat file
% (here dwi_0.6mm_b4kspace_hybrid_orientations.mat)
slide = 'P050x';

%Change the base to your current folder directory
base = '/vols/Data/km/srq306/submission/'
microfolder = [base,'/microscopy_PLI_P050x/tirl_output'];
dMRIfolder = [base,'/dMRI/'];
maskdir = [base,'/dMRI/data/nodif_brain_mask.nii.gz'];
dwiout = 'dwi_0.6mm_b4kspace';
smallregion = true;
create_hybrid_orientations(slide,microfolder,dMRIfolder,maskdir,dwiout,smallregion)

% Note, this script may take several hours to run. We have provided the
% output files in
% BigMac/Microscopy/PLI/Anterior/P089x/Reg2MRI/VectorMapping/HybridOrientations/dwi_0.6mm_b4kspace_hybrid_orientations.mat




%% Step 2

% Combine fibre orientations into a fibre orientation distribution at some
% specified voxel resolution (here 0.6mm isotropic).
% This will be run automatically at the end of Step 1 above.

% Inputs: hybrid orientations for each microcopy pixel e.g.
% Outputs: fibre orientation distribution stored as either spherical
% harmonic coefficients (two files, since fsl/mrtrix have L/R flip:
%                               dwi_0.6mm_b4kspace_SH_{fsl/mrtrix}.nii.gz
% or as counts over the 3D orientation histogram:
%                               dwi_0.6mm_b4kspace_count.nii.gz.


% To run this step alone, use a command similar to:
ddir = [base,'/microscopy_PLI_P050x/tirl_output/'];
maskfile = [base,'/dMRI/data/nodif_brain_mask.nii.gz'];
bdpxfolder= [base,'/dMRI/data.bedpostX/'];
res = 1.0;
smallregion = true;
mrtrixflag = false;

hybrid_nifti_SH(ddir,maskfile,res,res,bdpxfolder,dwiout,smallregion,mrtrixflag)


% The output can be visualised with FSLeyes
% e.g. navigate to
% ~/microscopy_PLI_P050x/tirl_output/HybridOrientations/res1mmX1mm
% and run the following command in the terminal
% fsleyes dwi_0.6mm_b4kspace_fa.nii.gz dwi_0.6mm_b4kspace_SH_fsl.nii.gz -ot sh -no -sr 10 -t 0


%% Step 3

% Combine information across slides (not veyr exciting here as we have only one
% slide)

% For each voxel, we count the total number of microscopy pixels across 
% slides (specified in 'dir256') for each vector in the frequency 
% histogram and output the normalised FOD, fitted using spherical harmonics.

% Input: slides to be combined, as listed in files.txt
% Output: whole brain nifti files
%                           dwi_0.6mm_b4kspace_count.nii.gz and
%                           dwi_0.6mm_b4kspace_SH_{fsl/mrtrix}.nii.gz

%hybrid_nifti_SH_combined(res,res,dwiout,mrtrixflag,'dir256','./',maskfile)

% Again this output can be visualised in FSLeyes/mrtrix
